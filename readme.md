Шаблон каталогов 1C проекта
===========================

clone.bat - видимо из хранилища 1С создает 


подключение к Вашему проекту
----------------------------

команды подключения к новому проекту

   git clone https://partizand@bitbucket.org/partizand/bootstrap1c.git ./PROJECTNAME


Для Github (репозиторий должен существовать)

> git remote set-url origin git@github.com:USERNAME/PROJECTNAME.git

Для BitBucket (репозиторий должен существовать)

> git remote set-url origin https://USERNAME@bitbucket.org/USERNAME/PROJECTNAME.git

И последнее

> git push -u origin --all

Описание каталогов
------------------

* devbase - каталог для разрабатываемой конфигурации (gitignore)
* storage - каталог для хранилища (gitignore)
* src - исходные тексты
* conf - для публикуемых конфигураций